class Event < ActiveRecord::Base
  validates :name,  presence: true, length: { maximum: 50 }
  validates_uniqueness_of :name
  has_many :jobs, dependent: :destroy
  has_many :event_job_job_associations, dependent: :destroy
  has_many :periods, through: :event_job_period_associations
  has_many :event_job_period_associations, dependent: :destroy
end
